from app import app, db
from models import User, Human, HumanStatus, Zombie, ZombieStatus, ZombieFaction
from datetime import datetime

def turn_humans():
    turned = Human.query.\
        filter(Human.status == HumanStatus.TURNING,
        Human.turned_at <= datetime.now()).\
        join(Human.user).join(Human.turned_by)
    
    for h in turned:
        db.session.delete(h)
        
        zombie = Zombie(h.user_id, ZombieStatus.UNDEAD,
            h.turned_by.faction, h.turned_at,
            h.turned_at + Zombie.STARVE_DELTA)
        
        db.session.add(zombie)
    
    db.session.commit()


def starve_zombies():
    starved = Zombie.query.\
        filter(Zombie.status == ZombieStatus.UNDEAD,
        Zombie.starves_at <= datetime.now()).\
        join(Zombie.user)
    
    for z in starved:
        z.status = ZombieStatus.STARVED
    
    db.session.commit()

