from app import app, db
from models import User, Human, HumanStatus, Zombie, ZombieStatus, ZombieFaction, Kill
from flask import render_template
from sqlalchemy.orm import joinedload
from datetime import datetime

@app.route('/')
def index():
    
    humans = Human.query.join(User).all()

    max_starve_time = Zombie.STARVE_DELTA.total_seconds()
    zombies = {}
    for faction in ZombieFaction:
        f_zombies = Zombie.query.filter(Zombie.faction == faction).join(User)\
            .options(joinedload(Zombie.kills).subqueryload(Kill.victim))
        zombies[faction] = sorted(f_zombies, key=lambda z: \
            z.starves_in().total_seconds() if z.status == ZombieStatus.UNDEAD \
            else max_starve_time + 1 if z.status == ZombieStatus.RESURECTED \
            else max_starve_time + 2)
    
    return render_template('index.html', humans=humans, zombie_factions=zombies.items())


@app.route('/seed')
def seed():
    
    u = User('Del', '148534942303911936')
    db.session.add(u)
    db.session.commit()

    a = User.query.filter(User.nickname == 'Del').scalar()
    z = Zombie(a.id, ZombieStatus.UNDEAD, datetime.now(), starves_at=datetime(2021, 5, 26))
    
    db.session.add(z)
    db.session.commit()
    
    return jsonify({ 'status': 'ok'})