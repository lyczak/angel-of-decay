from app import app
from jinja2 import Markup

@app.template_filter('turning_badge')
def _jinja2_filter_human_turning_badge(human):
    tdelta = human.turns_in()
    total_mins = tdelta.total_seconds() // 60
    hours = total_mins // 60
    mins = total_mins % 60
    return Markup('<span class="badge bg-{} rounded-pill">{:02n}:{:02n}</span>'.format(
        'danger' if hours < 12 else 'warning', hours, mins))


@app.template_filter('starving_badge')
def _jinja2_filter_zombie_starving_badge(zombie):
    tdelta = zombie.starves_in()
    total_mins = tdelta.total_seconds() // 60
    hours = total_mins // 60
    mins = total_mins % 60
    
    if hours >= 24:
        badge_status = 'success'
    elif hours >= 12:
        badge_status = 'warning'
    else:
        badge_status = 'danger'

    return Markup('<span class="badge bg-{} rounded-pill">{:02n}:{:02n}</span>'.format(
        badge_status, hours, mins))


@app.template_filter('kills_list')
def _jinja2_filter_zombie_kills_list(zombie):
    nicks = [kill.victim.nickname for kill in zombie.kills]
    if nicks:
        ln = len(nicks)
        return '{} kill{}: {}'.format(ln, 's' if ln != 1 else '', ', '.join(nicks))
    else:
        return '0 kills' 

