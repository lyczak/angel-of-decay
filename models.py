import enum
from datetime import datetime, timedelta
from app import db, config

class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(100))
    discord_id = db.Column(db.String(100))

    def __init__(self, nickname, discord_id):
        self.nickname = nickname
        self.discord_id = discord_id


class ZombieStatus(enum.Enum):
    UNDEAD = "Undead"
    RESURECTED = "Resurected"
    STARVED = "Starved"


class ZombieFaction(enum.Enum):
    ALPHA = "Alpha"
    OMEGA = "Omega"

    def get_role_id(self):
        return config['guild']['role_ids'][self.name]


class Zombie(db.Model):
    STARVE_DELTA = timedelta(hours=config['game']['time_to_starve'])

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    status = db.Column(db.Enum(ZombieStatus), nullable=False)
    faction = db.Column(db.Enum(ZombieFaction), nullable=False)
    turned_at = db.Column(db.DateTime, nullable=False)
    starves_at = db.Column(db.DateTime, nullable=True)

    user = db.relationship('User', foreign_keys='Zombie.user_id')
    kills = db.relationship('Kill', back_populates='killer', lazy=True)
    
    def __init__(self, user_id, status, faction, turned_at, starves_at=None):
        self.user_id = user_id
        self.status = status
        self.faction = faction
        self.turned_at = turned_at
        self.starves_at = starves_at
    
    def starves_in(self):
        starves_in = self.starves_at - datetime.now()
        return max(starves_in, timedelta(0))


class HumanStatus(enum.Enum):
    ALIVE = "Alive"
    TURNING = "Turning"
    RESURECTED = "Resurected"
    LEFT = "Left Campus"
    CONFIRM = "Confirm if Playing"


class Human(db.Model):
    TURN_DELTA = timedelta(hours=config['game']['time_to_turn'])
    
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    status = db.Column(db.Enum(HumanStatus), nullable=False)
    turned_at = db.Column(db.DateTime, nullable=True) # this should be in the future since it's 48 hrs after eaten
    turned_by_id = db.Column(db.Integer, db.ForeignKey(Zombie.id), nullable=True)
    
    turned_by = db.relationship('Zombie', foreign_keys='Human.turned_by_id')
    user = db.relationship('User', foreign_keys='Human.user_id')
    
    def __init__(self, user_id, status, turned_at=None, turned_by_id=None):
        self.user_id = user_id
        self.status = status
        self.turned_at = turned_at
        self.turned_by_id = turned_by_id
    
    def turns_in(self):
        turns_in = self.turned_at - datetime.now()
        return max(turns_in, timedelta(0))
    
    def eaten(self, turned_by_id):
        if self.status == HumanStatus.TURNING:
            return
        
        human.status = HumanStatus.TURNING
        self.turned_by_id = turned_by_id
        self.turned_at = datetime.now() + TURN_DELTA


class Kill(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    victim_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    killer_id = db.Column(db.Integer, db.ForeignKey(Zombie.id), nullable=False)
    killed_at = db.Column(db.DateTime, nullable=False)
    
    victim = db.relationship('User', foreign_keys='Kill.victim_id')
    killer = db.relationship('Zombie', foreign_keys='Kill.killer_id')
    
    def __init__(self, victim_id, killer_id, killed_at):
        self.victim_id = victim_id
        self.killer_id = killer_id
        self.killed_at = killed_at


if __name__ == "__main__":

    # Run this file directly to create the database tables.
    print("Creating database tables...")

    db.create_all()

    print("Seeding database tables...")

    d = User('Del', '148534942303911936')
    db.session.add(d)

    a = User('Artemis', '428234612112752641')
    db.session.add(a)
    db.session.commit()

    d = User.query.filter(User.nickname == 'Del').scalar()
    z = Zombie(d.id, ZombieStatus.UNDEAD, ZombieFaction.ALPHA, datetime(2021, 5, 21), datetime(2021, 5, 26))
    db.session.add(z)

    a = User.query.filter(User.nickname == 'Artemis').scalar()
    h = Human(a.id, HumanStatus.TURNING, datetime.now(), d.id)
    db.session.add(h)

    db.session.commit()

    print("Done!")