# Angel of Decay

> She watches from above with an unrelenting stare. Striking fear into hearts dead and undead, her presence knows no bounds. Make no mistake, she is watching you.

**Angel of Decay** is a combination Discord bot and web application designed to keep track of players and kills for the annual **Humans vs. Zombies** game at Lafayette College. This code is currently under active development and is not yet ready for production-use.

## Features

- [ ] Public scoreboard
    - [X] Humans' statuses and time-to-turn
    - [ ] Humans' attended events
    - [X] Zombies' statuses and time-to-starve
    - [X] Zombies' confirmed kills
    - [X] Separate lists for each zombie faction
- [ ] State management
    - [X] Turning humans become zombies
    - [X] Starving zombies become starved
- [ ] Discord slash-commands
    - [X] `/eat` command to eat a human
    - [ ] Game and role-management commands
    - [ ] Configuration commands
- [ ] Discord role-management
    - [ ] Human to zombie automatic assignment
    - [ ] Role-based command permissions