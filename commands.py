from discord_interactions.ocm import Command, Option, OptionChoices
from discord_interactions import ApplicationCommandOptionType


class Eat(Command):
    """ Eat a human """

    victim: ApplicationCommandOptionType.USER = Option("who did you eat?", required=True)
    time: str = Option("when did you eat?", required=True)
    location: str = Option("where did you eat?")
    
    