from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_apscheduler import APScheduler
import os
import logging
import json
import threading
import discord
from discord import Activity, ActivityType

# load config file and parse json
with open(os.getenv('AOD_CONFIG', 'config.json'), "r") as conf_file:
    config = json.load(conf_file)

app = Flask(__name__)
# app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SCHEDULER_API_ENABLED'] = False
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://' + config['web']['database_path']
app.config['SECRET_KEY'] = config['web']['app_secret']

# logging.basicConfig()
# logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

db = SQLAlchemy(app)
scheduler = APScheduler()
scheduler.init_app(app)
scheduler.start()

dclient = discord.Client()

@dclient.event
async def on_ready():
    await dclient.change_presence(activity=Activity(type=ActivityType.watching, name='the battlefield'))


def run_bot():
    dclient.run(config['bot']['bot_token'])


def run_app():
    app.run("0.0.0.0", config['web']['port'])


if __name__ == "__main__":
    
    from routes import *
    from filters import *
    from interactions import *
    
    from jobs import turn_humans, starve_zombies
    scheduler.add_job(func=turn_humans, trigger='interval',
        seconds=15, id='turn_humans', name='turn_humans', replace_existing=True)
    scheduler.add_job(func=starve_zombies, trigger='interval',
        seconds=15, id='starve_zombies', name='starve_zombies', replace_existing=True)
    
    threading.Thread(target=run_app).start()
    run_bot()

