from discord_interactions import ApplicationClient
import os

from app import config
from interactions import interactions

if __name__ == "__main__":
    client = ApplicationClient(config['bot']['bot_token'])

    client.bulk_overwrite_commands(interactions.commands, config['guild']['guild_id'])