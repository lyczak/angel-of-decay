from discord_interactions.flask_ext import Interactions
from discord_interactions import Member
import os
from app import app, db, config
from commands import Eat
from models import User, Human, Zombie

interactions = Interactions(app, config['bot']['public_key'], config['bot']['client_id'], path="/api/interactions")

@interactions.command
def eat(cmd: Eat):
    zombie_disc = cmd.interaction.author.id
    human_disc = cmd.victim

    zombie = Zombie.query.join(Zombie.user).filter(User.discord_id == zombie_disc)
    if zombie == None:
        return "Only zombies can use that command!", True

    human = Human.query.join(Human.user).filter(User.discord_id == human_disc).scalar()
    if human == None:
        return "You can only eat humans!", True

    human.eaten(zombie.id)
    db.session.commit()

    return f"<@{zombie_disc}> ate <@{human_disc}> at {cmd.time}!"

